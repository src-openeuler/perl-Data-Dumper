%define mod_name Data-Dumper
Name:     perl-%{mod_name}
Version:  2.183
Release:  3
Summary:  Stringified perl data structures, suitable for both printing and eval  
License:  GPL-1.0-or-later OR Artistic-1.0-Perl
URL:      https://metacpan.org/release/%{mod_name}
Source0:  https://cpan.metacpan.org/authors/id/X/XS/XSAWYERX/%{mod_name}-%{version}.tar.gz

BuildRequires:  make findutils perl-generators perl-interpreter perl(ExtUtils::MakeMaker) >= 6.76 perl(Test::More) >= 0.98
BuildRequires:  perl-devel gcc
Requires:  perl(B::Deparse) perl(bytes) perl(Scalar::Util) perl(XSLoader)

%description
Given a list of scalars or reference variables, writes out their contents in perl syntax. 
The references can also be objects. The content of each variable is output in a single Perl statement. 
Handles self-referential structures correctly.
The return value can be evaled to get back an identical copy of the original reference structure. 
(Please do consider the security implications of eval'ing code from untrusted sources!)

%package_help

%prep
%autosetup -n %{mod_name}-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PERLLOCAL=1 NO_PACKLIST=1 OPTIMIZE="%{optflags}"
%make_build

%install
%make_install
%{_fixperms} %{buildroot}/*

%check
make test

%files
%doc Changes
%{perl_vendorarch}/*

%files help
%doc Todo
%{_mandir}/*/*

%changelog
* Fri Jan 17 2025 Funda Wang <fundawang@yeah.net> - 2.183-3
- drop useless perl(:MODULE_COMPAT) requirement

* Tue Oct 25 2022 zhoushuiqing <zhoushuiqing2@huawei.com> - 2.183-2
- define mod_name to opitomize the specfile

* Mon Nov 22 2021 shixuantong <shixuantong@huawei.com> - 2.183-1
- update version to 2.183

* Tue Jun 29 2021 wuchaochao <wuchaochao4@huawei.com> - 2.173-2
- add buildrequers gcc

* Thu Jul 23 2020 shixuantong <shixuantong@huawei.com> - 2.173-1
- update to 2.173-1

* Wed May 13 2020 openEuler Buildteam <buildteam@openeuler.org> - 2.172-4
- Add build requires of perl-devel

* Sat Dec 21 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.172-3
- Change mod of files

* Mon Sep 2 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.172-2 
- Package init
